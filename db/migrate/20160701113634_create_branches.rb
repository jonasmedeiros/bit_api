class CreateBranches < ActiveRecord::Migration
  def change
    create_table :branches do |t|
      t.string :local
      t.string :phone
      t.string :email
      t.string :cordinate

      t.timestamps null: false
    end
  end
end
