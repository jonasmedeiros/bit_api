class AddDistrictToBranches < ActiveRecord::Migration
  def change
    add_column :branches, :district, :string
  end
end
