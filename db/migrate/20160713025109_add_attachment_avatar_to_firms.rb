class AddAttachmentAvatarToFirms < ActiveRecord::Migration
  def self.up
    change_table :firms do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :firms, :avatar
  end
end
