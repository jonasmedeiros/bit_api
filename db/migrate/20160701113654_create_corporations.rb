class CreateCorporations < ActiveRecord::Migration
  def change
    create_table :corporations do |t|
      t.boolean :active
      t.string :title
      t.string :url

      t.timestamps null: false
    end
  end
end
