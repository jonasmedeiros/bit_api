class AddAttachmentAvatarToCarrers < ActiveRecord::Migration
  def self.up
    change_table :carrers do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :carrers, :avatar
  end
end
