class AddColumnsAtBranches < ActiveRecord::Migration
  def change
    add_column :branches, :title, :string
    add_column :branches, :street, :string
    add_column :branches, :number, :string
    add_column :branches, :latitude, :string
    add_column :branches, :longitude, :string
  end
end