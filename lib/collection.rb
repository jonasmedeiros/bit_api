class Collection
  attr_accessor :items

  def initialize(list)
    
    self.items = []

    list.each do |data|
      self.items << data
    end

  end
end
