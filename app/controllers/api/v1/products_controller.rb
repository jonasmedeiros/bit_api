class Api::V1::ProductsController < Api::V1::BaseController
  
  include Roar::Rails::ControllerAdditions
  include Roar::Rails::ControllerAdditions::Render

  def index
    products = Product.all
    collection = Collection.new(products)

    render json: collection, represent_with: ProductsRepresenter,  status: 200
  end

  def show
    product = Product.find(params[:id])

    render json: product, represent_with: ProductRepresenter,  status: 200
  end
end