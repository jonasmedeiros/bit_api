class Api::V1::ContactsController < Api::V1::BaseController
  
  def index
    @contact = Contact.create( contact_params )

    render :json => @contact
  end

  private

  # Use strong_parameters for attribute whitelisting
  # Be sure to update your create() and update() controller methods.

  def contact_params
    params.require(:contact).permit(:name, :email, :phone, :message)
  end
end