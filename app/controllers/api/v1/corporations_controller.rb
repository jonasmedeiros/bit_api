class Api::V1::CorporationsController < Api::V1::BaseController
  
  include Roar::Rails::ControllerAdditions
  include Roar::Rails::ControllerAdditions::Render

  def index
    corporations = Corporation.all
    collection = Collection.new(corporations)

    render json: collection, represent_with: CorporationsRepresenter,  status: 200
  end

  def show
    corporation = Corporation.find(params[:id])

    render json: corporation, represent_with: CorporationRepresenter,  status: 200
  end
end