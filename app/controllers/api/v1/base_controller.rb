class Api::V1::BaseController < ApplicationController

  protect_from_forgery with: :null_session

  before_action :destroy_session

  after_filter :set_access_control_headers

  def set_access_control_headers

    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Request-Method'] = '*'

  end

  def destroy_session

    request.session_options[:skip] = true
    
  end
end