class Api::V1::NewslettersController < Api::V1::BaseController
  
  def index
    @newsletter = Newsletter.create( newsletter_params )

    render :json => @newsletter
  end

  private

  # Use strong_parameters for attribute whitelisting
  # Be sure to update your create() and update() controller methods.

  def newsletter_params
    params.require(:newsletter).permit(:email, :name)
  end
end