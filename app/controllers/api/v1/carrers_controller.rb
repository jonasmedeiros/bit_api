class Api::V1::CarrersController < Api::V1::BaseController
  
  def index
    @carrer = Carrer.create( carrer_params )

    render :json => @carrer
  end

  private

  # Use strong_parameters for attribute whitelisting
  # Be sure to update your create() and update() controller methods.

  def carrer_params
    params.require(:carrer).permit(:name, :email, :phone, :avatar)
  end
end