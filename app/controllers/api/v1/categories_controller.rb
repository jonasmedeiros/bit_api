class Api::V1::CategoriesController < Api::V1::BaseController
  
  include Roar::Rails::ControllerAdditions
  include Roar::Rails::ControllerAdditions::Render

  def index
    categories = Category.all
    collection = Collection.new(categories)

    render json: collection, represent_with: CategoriesRepresenter,  status: 200
  end

  def show
    category = Category.find(params[:id])

    render json: category, represent_with: CategoryRepresenter,  status: 200

  end
end