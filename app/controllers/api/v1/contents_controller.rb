class Api::V1::ContentsController < Api::V1::BaseController
  
  include Roar::Rails::ControllerAdditions
  include Roar::Rails::ControllerAdditions::Render

  def index
    contents = Content.all
    collection = Collection.new(contents)

    render json: collection, represent_with: ContentsRepresenter,  status: 200
  end

  def show
    content = Content.find(params[:id])

    render json: content, represent_with: ContentRepresenter,  status: 200
  end
end