class Api::V1::BannersController < Api::V1::BaseController
  
  include Roar::Rails::ControllerAdditions
  include Roar::Rails::ControllerAdditions::Render

  def index
    banners = Banner.all
    collection = Collection.new(banners)

    render json: collection, represent_with: BannersRepresenter,  status: 200
  end

  def show
    banner = Banner.find(params[:id])

    render json: banner, represent_with: BannerRepresenter,  status: 200
  end
end