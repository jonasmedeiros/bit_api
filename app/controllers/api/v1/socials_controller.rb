class Api::V1::SocialsController < Api::V1::BaseController
  
  include Roar::Rails::ControllerAdditions
  include Roar::Rails::ControllerAdditions::Render

  def index
    socials = Social.all
    collection = Collection.new(socials)

    render json: collection, represent_with: SocialsRepresenter,  status: 200
  end

  def show
    social = Social.find(params[:id])

    render json: social, represent_with: SocialRepresenter,  status: 200
  end
end