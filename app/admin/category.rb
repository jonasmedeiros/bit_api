ActiveAdmin.register Category do

  permit_params :title, :avatar, :description
  menu parent: "Site", priority: 1, label: "Category"

  filter :title

  index do
    selectable_column
    column :id
    column :title
    actions
  end

  form do |f|
    f.inputs 'Category' do
      f.input :title
      f.input :description, as: :html_editor
      f.input :avatar, :as => :file
    end
    f.button 'Save'
  end

end
