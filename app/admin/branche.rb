ActiveAdmin.register Branche do

  permit_params :title, :street, :district,  :number, :phone, :email, :latitude, :longitude
  menu parent: "Site", priority: 1, label: "Branches"

  filter :phone
  filter :email

  index do
    selectable_column
    column :id
    column :phone
    column :email
    column :street
    actions
  end

  form do |f|
    f.inputs 'Branche' do
      f.input :title
      f.input :street
      f.input :district
      f.input :number
      f.input :phone
      f.input :email
      f.input :latitude
      f.input :longitude
    end
    f.button 'Save'
  end


end
