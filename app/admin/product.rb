ActiveAdmin.register Product do

  permit_params :title, :description, :avatar, :category_id
  menu parent: "Site", priority: 1, label: "Products"

  filter :title
  filter :description

  index do
    selectable_column
    column :id
    column :title
    column :category
    actions
  end

  form do |f|
    f.inputs 'Product' do
      f.input :category
      f.input :title
      f.input :description, as: :html_editor
      f.input :avatar, :as => :file
    end
    f.button 'Save'
  end



end
