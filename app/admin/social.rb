ActiveAdmin.register Social do

  permit_params :icon, :title, :url
  menu parent: "Site", priority: 1, label: "Social"

  filter :icon
  filter :title
  filter :url

  index do
    selectable_column
    column :id
    column :icon
    column :title
    column :url
    actions
  end

  form do |f|
    f.inputs 'Social' do
      f.input :icon
      f.input :title
      f.input :url
    end
    f.button 'Save'
  end


end
