ActiveAdmin.register Contact do

  permit_params :name, :email, :phone, :message
  menu parent: "Site", priority: 1, label: "Contact"

  filter :name

  index do
    selectable_column
    column :id
    column :name
    column :email
    actions
  end

  form do |f|
    f.inputs 'Contact' do
      f.input :name
      f.input :email
      f.input :phone
      f.input :message
    end
    f.button 'Save'
  end

end
