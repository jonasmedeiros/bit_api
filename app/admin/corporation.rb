ActiveAdmin.register Corporation do

  permit_params :active, :title, :url
  menu parent: "Site", priority: 1, label: "Corporation"

  filter :title
  filter :url

  index do
    selectable_column
    column :id
    column :title
    column :url
    actions
  end

  form do |f|
    f.inputs 'Corporation' do
      f.input :active
      f.input :title
      f.input :url
    end
    f.button 'Save'
  end

end
