module CategoriesRepresenter
  include Roar::JSON
  include Roar::Hypermedia
  
  collection :items, extend: CategoryRepresenter, as: "categories"
end
