module CorporationRepresenter
  include Roar::JSON
  include Roar::Hypermedia
  
  property :id
  property :active
  property :title
  property :url

  link :self do
    
  end
end
