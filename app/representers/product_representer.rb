module ProductRepresenter
  include Roar::JSON
  include Roar::Hypermedia
  
  property :id
  property :title  
  property :description
  property :avatar, extend: AssetRepresenter

  property :category, extend: CategoryRepresenter

  link :self do
    
  end
end
