module BrancheRepresenter
  include Roar::JSON
  include Roar::Hypermedia

  property :id
  property :title
  property :street
  property :district
  property :number
  property :phone
  property :email
  property :cordinate
  property :latitude
  property :longitude

  link :self do
    
  end
end
