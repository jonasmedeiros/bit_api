module AssetRepresenter
  include Roar::JSON
  include Roar::Hypermedia

  property :default
  property :medium
  property :thumb

  def default
    represented.url
  end

  def thumb
    represented.url(:thumb)
  end

  def medium
    represented.url(:medium)
  end
  
end
