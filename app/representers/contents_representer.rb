module ContentsRepresenter
  include Roar::JSON
  include Roar::Hypermedia
  
  collection :items, extend: ContentRepresenter, as: "contents"
end
