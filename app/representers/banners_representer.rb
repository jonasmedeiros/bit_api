module BannersRepresenter
  include Roar::JSON
  include Roar::Hypermedia
  
  collection :items, extend: BannerRepresenter, as: "banners"
end
